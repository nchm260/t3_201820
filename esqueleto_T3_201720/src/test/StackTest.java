package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOByke;

public class StackTest 
{
	private Stack fila;
	private VOByke elem1;
	private VOByke elem2;
	private VOByke elem3;

	@SuppressWarnings("unchecked")
	private void setupEscenario1()
	{
		fila = new Stack();
		elem1 = new VOByke(123);
		elem2 = new VOByke(234);
		elem3 = new VOByke(456);
		fila.push(elem1);
		fila.push(elem2);
		fila.push(elem3);
	}

	@Test
	public void pushTest ()
	{
		setupEscenario1();
		assertNotNull("Error en enqueue" , fila.buscar(123));
		assertNotNull("Error en enqueue", fila.buscar(234));
		assertNotNull("Error en enqueue", fila.buscar(456));
		assertNull("Error en enqueue", fila.buscar(124));
		
	}
	@Test
	public void popTest()
	{
		setupEscenario1();
		assertEquals("Error en dequeue", 456, fila.pop().hashCode());
		assertEquals("Error en dequeue", 234, fila.pop().hashCode());
		assertEquals("Error en dequeue", 123, fila.pop().hashCode());
		
	}

}
