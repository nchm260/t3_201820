package test;

import model.data_structures.Queue;
import model.vo.VOByke;
import model.vo.VOTrip;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.Assert;
import org.junit.Test;

public class QueueTest
{
	private Queue cola;
	private VOByke elem1;
	private VOByke elem2;
	private VOByke elem3;

	@SuppressWarnings("unchecked")
	private void setupEscenario1()
	{
		cola = new Queue();
		elem1 = new VOByke(123);
		elem2 = new VOByke(234);
		elem3 = new VOByke(456);
		cola.enqueue(elem1);
		cola.enqueue(elem2);
		cola.enqueue(elem3);
	}

	@Test
	public void enqueueTest ()
	{
		setupEscenario1();
		assertNotNull("Error en enqueue" , cola.buscar(123));
		assertNotNull("Error en enqueue", cola.buscar(234));
		assertNotNull("Error en enqueue", cola.buscar(456));
		assertNull("Error en enqueue", cola.buscar(124));
		
	}
	@Test
	public void dequeueTest()
	{
		setupEscenario1();
		assertEquals("Error en dequeue", 123, cola.dequeue().hashCode());
		assertEquals("Error en dequeue", 234, cola.dequeue().hashCode());
		assertEquals("Error en dequeue", 456, cola.dequeue().hashCode());
		
	}
}
