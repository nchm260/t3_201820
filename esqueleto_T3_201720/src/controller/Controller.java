package controller;

import api.IDivvyTripsManager;
import model.data_structures.Queue;
import model.logic.DivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOStation;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() 
	{
		manager.loadServices("./esqueleto_T3_201720/data/Divvy_Stations_2017_Q3Q4.csv");
	}
	
	public static void loadTrips() 
	{
		manager.loadTrips("./esqueleto_T3_201720/data/Divvy_Trips_2017_Q3.csv");
	}

	public static Queue<VOStation> getLastNStations(int bikeId,	int	n) 
	{
		return manager.getLastNStations(bikeId, n);
	}
	
	public VOTrip customerNumberN(int stationId,	int	n) 
	{
		return manager.customerNumberN(stationId, n);
	}
}
