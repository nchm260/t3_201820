package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.ILinkedList;
import model.data_structures.Queue;
import model.vo.VOByke;
import model.vo.VOStation;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		Controller control = new Controller();
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();


			switch(option)
			{
			case 1:
				Controller.loadStations();
				break;

			case 2:
				Controller.loadTrips();
				break;

			case 3:
				System.out.println("Ingrese el id de la bicicleta:");
				String id = sc.next();
				System.out.println("Ingrese el número deseado de estaciones:");
				String n = sc.next();
				Queue<VOStation> stationsQueue = control.getLastNStations(Integer.valueOf(id),Integer.valueOf(n));
				while(stationsQueue.getSize() != 0)
				{
					System.out.println(stationsQueue.dequeue().darNombre());
				}
				break;

			case 4:
				System.out.println("Ingrese el identificador de la estación:");
				int stationId = Integer.parseInt(sc.next());
				System.out.println("Ingrese el número de trip deseado:");
				int m = Integer.valueOf(sc.next());
				VOTrip resp = control.customerNumberN(stationId, m);
				System.out.println("Se encontró el siguiente viaje:" + resp.toString());
				break;

			case 5:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cree una nueva coleccion de estaciones");
		System.out.println("2. Cree una nueva coleccion de viajes");
		System.out.println("3. Dar listado de las ultimas estaciones de una bicicleta");
		System.out.println("4. Dar información del 'n'-esimo viaje");
		System.out.println("5. Salir");
		System.out.println("Digite el número de opción para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}
}
