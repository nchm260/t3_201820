package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager 
{

	Stack<VOTrip> tripsStack;
	Queue<VOTrip> tripsQueue;
	
	Stack<VOStation> stationsStack;
	Queue<VOStation> stationsQueue;
	
	
	public DivvyTripsManager() 
	{
		tripsStack=new Stack<VOTrip>();
		tripsQueue=new Queue<VOTrip>();
		stationsStack=new Stack<VOStation>();
		stationsQueue=new Queue<VOStation>();
	}
	
	
	public void loadStations (String stationsFile) throws IOException 
	{
		
		File archivo=new File(stationsFile);
		FileReader fr=new FileReader(archivo);
		BufferedReader bf=new BufferedReader(fr);
		bf.readLine();
		String datos=bf.readLine();
		while(datos!=null) 
		{
			String[] info= datos.replaceAll("\"", "").split(",");
			int id=Integer.parseInt(info[0]);
			String name=info[1];
			String city=info[2];
			double latitude=Double.parseDouble(info[3]);
			double longitude=Double.parseDouble(info[4]);
			int capacity=Integer.parseInt(info[5]);
			Date date=new Date(info[6]);
			VOStation nuevo=new VOStation(id, name, city, latitude, longitude, capacity, date);
			stationsStack.push(nuevo);
			stationsQueue.enqueue(nuevo);
			datos=bf.readLine();
		}
		fr.close();
		bf.close();
		
	}

	
	public void loadTrips (String tripsFile) 
	{
		File archivo=new File(tripsFile);
		FileReader fr;
		try
		{
			fr = new FileReader(archivo);
			BufferedReader bf=new BufferedReader(fr);
			bf.readLine();
			String datos=bf.readLine();
			while(datos!=null) 
			{
				String[] info=datos.replaceAll("\"", "").split(",");
				int id=Integer.parseInt(info[0]);
				Date start_time=new Date(info[1]);
				Date end_time=new Date(info[2]);
				VOByke byke=new VOByke(Integer.parseInt(info[3]));
				double duration=Double.parseDouble(info[4]);
				VOStation originStation=new VOStation(Integer.parseInt(info[5]), info[6]);
				VOStation destinationStation=new VOStation(Integer.parseInt(info[7]), info[8]);
				String usertype=info[9];
				String gender="";
				if(info.length>=11) 
				{
					gender=info[10];
				}
				int birthyear=0;
				
				if(info.length>=12) 
				{
					birthyear=Integer.parseInt(info[11]);
				}
				VOTrip nuevo= new VOTrip(id, start_time, end_time, byke, duration, originStation, destinationStation, usertype, gender, birthyear);
				tripsStack.push(nuevo);
				tripsQueue.enqueue(nuevo);
				datos=bf.readLine();
				
			}
			fr.close();
			bf.close();
			
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	}
	


	@Override
	public void loadServices(String stationsFile) 
	{
		File archivo=new File(stationsFile);
		try {
		FileReader fr=new FileReader(archivo);
		BufferedReader bf=new BufferedReader(fr);
		bf.readLine();
		String datos=bf.readLine();
		while(datos!=null) {
			String[] info=datos.split(",");
			int id=Integer.parseInt(info[0]);
			String name=info[1];
			String city=info[2];
			double latitude=Double.parseDouble(info[3]);
			double longitude=Double.parseDouble(info[4]);
			int capacity=Integer.parseInt(info[5]);
			Date date=new Date(info[6]);
			VOStation nuevo=new VOStation(id, name, city, latitude, longitude, capacity, date);
			stationsStack.push(nuevo);
			
			
			datos=bf.readLine();
		}
		fr.close();
		bf.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}

	public Queue<VOStation> getLastNStations(int bikeId,	int	n)
	{
		Queue<VOStation> elegidos=new Queue<VOStation>();
		VOTrip actual=tripsQueue.dequeue();
		while(actual!=null) 
		{
			
			if(actual.getByke().id()==bikeId) 
			{
				elegidos.enqueue(actual.getToStation());
				
				//Si se adicionan mas estaciones de las esperadas se retira mas antigua en meterse
				
				if(elegidos.getSize()>n)
					elegidos.dequeue();

				actual=tripsQueue.dequeue();
			}
		}

		return elegidos;
	}
	

	
	public VOTrip customerNumberN(int stationId,	int	n)
	{
		
		Stack<VOTrip> buscados=new Stack<VOTrip>();
		VOTrip buscado=null;
		VOTrip actual=tripsStack.pop();
		while(actual!=null) {
				if(actual.getByke().id()==stationId) 
				{
					buscados.push(actual);

					actual=tripsStack.pop();
				}
		}		
		//Como se metieron en desorden, el stack me permite sacarlos en orden asi consigo el n-esimo
		for(int i=0;i<n && buscados.getSize()>0;i++) 
		{
			buscado=buscados.pop();
		}
		
		return buscado;
	}
	
}
