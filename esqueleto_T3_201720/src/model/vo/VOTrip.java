package model.vo;

import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int id;
	
	private Date start_time;
	
	private Date end_time;
	
	private VOStation originStation;
	
	private VOStation destinationStation;
	
	private VOByke byke;
	
	private double duration;
	
	private String usertype;
	
	private String gender;
	
	private int birthyear;
	
	
	public VOTrip(int id, Date start_time, Date end_time, VOByke byke, double duration, VOStation originStation, VOStation destinationStation, String usertype, String gender, int birthyear) {
		this.id=id;
		this.start_time=start_time;
		this.end_time=end_time;
		this.byke=byke;
		this.duration=duration;
		this.originStation=originStation;
		this.destinationStation=destinationStation;
		this.usertype=usertype;
		this.gender=gender;
		this.birthyear=birthyear;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return id;
	}	


	/**
	 * @return station_name - Origin Station Name .
	 */
	public VOStation getFromStation() {
		return originStation;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public VOStation getToStation() {
		return destinationStation;
	}
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		return duration;
	}
	
	public String getGender() {
		return gender;
	}
	
	public VOByke getByke() {
		return byke;
	}
	@Override
	public String toString()
	{
		return id + " "+ start_time + " "+ end_time + " " + originStation + " " + destinationStation +  " " + byke + " " + duration + " " + usertype + " " + gender + " " + birthyear +".";
	}
}
