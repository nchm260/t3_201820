package model.data_structures;

public class Queue<E> extends LinkedList<E> implements IQueue<E> 
{

	public Queue() 
	{
		super();
	}
	
	@Override
	public void enqueue(E item) 
	{
		addFinal(item);
	}

	@Override
	public E dequeue() 
	{
		E eliminado=null;
		if(primero!=null) 
		{
			eliminado=primero.darElemento();
			primero=primero.darSiguiente();
		}
		return eliminado;
	}
	
}
