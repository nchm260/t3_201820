package model.data_structures;

import java.util.Iterator;

public class LinkedList<E> implements ILinkedList<E> 
{

	protected Node<E> primero;

	protected int size;

	public LinkedList(){
		primero=null;
		size=0;
	}
	public void addPrincipio(E elemento) {
		Node<E> nuevo =new Node<E>(elemento);
		if(primero==null) {
			primero=nuevo;
			size=1;
		}else {
			nuevo.cambiarSiguiente(primero);
			primero=nuevo;
			size++;
		}
	}

	/**
	 * añade un elemento a la lista al final de esta
	 * @param elemento elemento a anadir
	 */
	public void addFinal(E elemento) {
		Node<E> nuevo =new Node<E>(elemento);
		if(primero==null) {
			primero=nuevo;
			size=1;
		}else {
			Node<E> actual=primero;
			while(actual.darSiguiente()!=null) {
				actual=actual.darSiguiente();
			}
			actual.cambiarSiguiente(nuevo);
			size++;
		}
	}

	@Override
	public Iterator<E> iterator() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSize() 
	{
		return size;
	}

	public E buscar (int id)
	{
		E elem = null;
		Node<E> actual = primero;
		if (actual != null)
		{	
			boolean termino = false;
			while(actual.darSiguiente() != null && !termino)	
			{
				if (actual.darElemento().hashCode() == id)
				{
					elem = actual.darElemento();
					termino = true;
				}
			}
		}
		return elem;
	}

}
