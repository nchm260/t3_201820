package model.data_structures;

import java.util.Iterator;

public class Stack<E> extends LinkedList<E> implements IStack<E> {

	public Stack() {
		super();
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public void push(E item) {
		addPrincipio(item);
	}

	@Override
	public E pop() {
		E eliminado=null;
		if(primero!=null) {
			eliminado=primero.darElemento();
			primero=primero.darSiguiente();
		}
		return eliminado;
	}
	
}
