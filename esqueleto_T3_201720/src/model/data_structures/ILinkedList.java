package model.data_structures;

public interface ILinkedList<E> extends Iterable<E> {
	
	public int getSize();
	
}
